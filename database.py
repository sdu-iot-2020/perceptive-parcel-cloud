from os import environ
from typing import List, Dict, Tuple

from flask import Flask
from rdflib import plugin, Namespace, URIRef, BNode, Literal, RDF, XSD
from rdflib.graph import Graph
from rdflib.plugins.sparql import prepareQuery
from rdflib.plugins.sparql.parser import Query
from rdflib.store import Store
from rdflib_sqlalchemy import registerplugins

DEVICES = Namespace('http://example.com/owl/devices/')


class Database:
    DATABASE_URL: str = environ['DATABASE_URL']

    prepared_query_device: Query = prepareQuery("""
        prefix : <http://example.com/owl/devices/> 
        prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT ?time ?la ?lo ?temp ?hum ?shake ?tilt
        WHERE {
            ?d rdf:type :Device .
            ?d :hasId ?id .
            ?d :hasMeasurement ?m .
            ?m :hasTime ?time .
            ?m :hasTemperature ?temp .
            ?m :hasHumidity ?hum.
            ?m :isTilted ?tilt.
            ?m :isShaken ?shake.
            ?m :hasGeolocation ?g .
            ?g :hasLatitude ?la .
            ?g :hasLongitude ?lo .
        }
    """)

    def __init__(self, app: Flask) -> None:
        self.app = app
        registerplugins()

        self.app.logger.debug(self.DATABASE_URL)

        self.store = plugin.get("SQLAlchemy", Store)(identifier="my_store")
        self.graph = Graph(self.store, identifier="my_graph")

        self.graph.open(self.DATABASE_URL, create=True)
        self.graph.parse('ontology.ttl', format='turtle')

    def close(self):
        self.graph.close()

    def insert(self, triples: List[Tuple]) -> None:
        for triple in triples:
            self.graph.add(triple)
        self.graph.commit()

    def insert_measurement(self, uplink: Dict) -> None:
        id = URIRef(f"http://example.com/owl/devices/{uplink['id']}")

        measurement = BNode()
        geolocation = BNode()

        triples = [
            (id, RDF.type, DEVICES['Device']),
            (id, DEVICES['hasId'], Literal(uplink['id'], datatype=XSD.string)),
            (id, DEVICES['hasMeasurement'], measurement),
            (measurement, RDF.type, DEVICES['Measurement']),
            (measurement, DEVICES['hasTime'], Literal(uplink['time'], datatype=XSD.long)),
            (measurement, DEVICES['hasTemperature'], Literal(uplink['temperature'], datatype=XSD.integer)),
            (measurement, DEVICES['hasHumidity'], Literal(uplink['humidity'], datatype=XSD.integer)),
            (measurement, DEVICES['isTilted'], Literal(uplink['tilted'], datatype=XSD.boolean)),
            (measurement, DEVICES['isShaken'], Literal(uplink['shaken'], datatype=XSD.boolean)),
            (measurement, DEVICES['hasGeolocation'], geolocation),
            (geolocation, DEVICES['hasLatitude'], Literal(uplink['geolocation']['lat'], datatype=XSD.double)),
            (geolocation, DEVICES['hasLongitude'], Literal(uplink['geolocation']['lng'], datatype=XSD.double))
        ]

        self.insert(triples)

    def query_devices(self) -> List[str]:
        result = self.graph.query("""
            prefix : <http://example.com/owl/devices/> 
            prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            SELECT ?id
            WHERE {
                ?x rdf:type :Device .
                ?x :hasId ?id .
            }
        """)
        self.graph.commit()
        return [row.id.toPython() for row in result]

    def query_device(self, ids: str) -> List[Tuple]:
        result = self.graph.query(self.prepared_query_device, initBindings={'id': ids})
        self.graph.commit()
        return [(row.time.toPython(), row.la.toPython(),
                 row.lo.toPython(), row.temp.toPython(),
                 row.hum.toPython(), row.shake.toPython(), row.tilt.toPython()) for row in result]
