from logging import getLogger
from os import environ
from typing import Tuple

import requests
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

from database import Database

app = Flask(__name__)
db = Database(app)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type',

MAILGUN_API_KEY = environ['MAILGUN_API_KEY']
MAILGUN_DOMAIN = environ['MAILGUN_DOMAIN']
MAILGUN_API_URL: str = f'https://api.mailgun.net/v3/{MAILGUN_DOMAIN}/messages'

queue = set()


@app.route('/test', methods=['GET'])
@cross_origin()
def test() -> str:
    requests.post(
        MAILGUN_API_URL,
        auth=('api', MAILGUN_API_KEY),
        data={'from': f'Excited User <mailgun@{MAILGUN_DOMAIN}>',
              'to': 'emiltangkristensen@gmail.com',
              'subject': 'Mailgun Test',
              'text': 'Mailgun Test'}
    )
    return ''


@app.route('/devices', methods=['GET'])
@cross_origin()
def devices() -> str:
    result = db.query_devices()
    app.logger.debug(result)
    return jsonify(result)


@app.route('/devices/<device_id>', methods=['GET'])
@cross_origin()
def device(device_id: str) -> str:
    result = db.query_device(device_id)
    return jsonify({
        'id': device_id,
        'measurements': [
            {
                'time': time,
                'lat': lat,
                'lng': lng,
                'temp': temp,
                'hum': hum,
                'shake': shake,
                'tilt': tilt
            } for time, lat, lng, temp, hum, shake, tilt in result
        ]
    })


@app.route('/uplink', methods=['POST'])
def uplink() -> str:
    measurement = request.get_json()
    app.logger.debug(measurement)
    if measurement['tilted'] or measurement['shaken']:
        tilted = 'Device has been tilted\n\n' if measurement['tilted'] else ''
        shaken = 'Device has been shaken\n\n' if measurement['shaken'] else ''
        requests.post(
            MAILGUN_API_URL,
            auth=('api', MAILGUN_API_KEY),
            data={'from': f'Excited User <mailgun@{MAILGUN_DOMAIN}>',
                  'to': 'emiltangkristensen@gmail.com',
                  'subject': 'Parcel Warning',
                  'text': f"Parcel: {measurement['id']}\n\n{tilted}{shaken}"}
        )
        db.insert_measurement(measurement)
    return ''


@app.route('/downlink', methods=['POST'])
@cross_origin()
def dowlink() -> Tuple[str, int]:
    json = request.get_json()
    app.logger.debug(json)
    device_id = json['deviceId']
    # if device_id in queue:
    # queue.remove(device_id)
    response = jsonify({device_id: {'downlinkData': "deadbeefcafebabe"}})
    app.logger.debug(response)
    return response, 200
    # else:
    #    return '', 204


@app.route('/locate', methods=['POST'])
@cross_origin()
def locate() -> str:
    json = request.get_json()
    queue.add(json['deviceId'])
    return ''


if __name__ != '__main__':
    gunicorn_logger = getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.logger.debug('Debug Log Active')

elif __name__ == '__main__':
    app.run()
